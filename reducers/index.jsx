import { combineReducers } from 'redux';

import libsState from './libraries';
import libState from './library';
import compState from './computers';

const appReducer = combineReducers({
    libsState,
    libState,
    compState
});

export default appReducer;